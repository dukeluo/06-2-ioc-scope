package com.twuc.webApp;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import java.util.Random;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PrototypeDependentWithProxy {

    private int random = new Random().nextInt(1000);

    public int getRandom() {
        return random;
    }

    public PrototypeDependentWithProxy getThis() {
        return this;
    }

}



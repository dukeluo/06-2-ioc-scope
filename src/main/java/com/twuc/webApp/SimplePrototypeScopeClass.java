package com.twuc.webApp;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class SimplePrototypeScopeClass {
    private List<String> logger = new ArrayList<String>();

    public SimplePrototypeScopeClass() {
        logger.add(String.format("SimplePrototypeScopeClass() %d", new Date().getTime()));
    }

    public List<String> getLogger() {
        return logger;
    }
}

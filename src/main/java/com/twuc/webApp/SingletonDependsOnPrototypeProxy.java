package com.twuc.webApp;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {

    private PrototypeDependentWithProxy prototypeDependentWithProxy;

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }
}

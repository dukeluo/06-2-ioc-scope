package com.twuc.webApp;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class InterfaceOneImpl implements InterfaceOne {
    private List<String> logger = new ArrayList<String>();

    public InterfaceOneImpl() {
        logger.add(String.format("SimplePrototypeScopeClass() %d", new Date().getTime()));
    }

    public List<String> getLogger() {
        return logger;
    }
}

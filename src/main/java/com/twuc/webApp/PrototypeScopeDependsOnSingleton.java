package com.twuc.webApp;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
public class PrototypeScopeDependsOnSingleton {

    private SingletonDependent singletonDependent;

    public PrototypeScopeDependsOnSingleton(SingletonDependent singletonDependent) {
        this.singletonDependent = singletonDependent;
    }

    public SingletonDependent getSingletonDependent() {
        return singletonDependent;
    }
}

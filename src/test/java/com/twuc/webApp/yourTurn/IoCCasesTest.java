package com.twuc.webApp.yourTurn;

import com.twuc.webApp.*;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.*;

public class IoCCasesTest {

    // 2.1.1
    @Test
    void should_get_the_same_instance_with_the_interface() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        InterfaceOneImpl beanOne = context.getBean(InterfaceOneImpl.class);
        InterfaceOne beanTwo = context.getBean(InterfaceOne.class);

        assertNotNull(beanOne);
        assertNotNull(beanTwo);
        assertEquals(beanOne, beanTwo);
    }

    // 2.1.2
    @Test
    void should_get_the_same_instance_with_the_super_class() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        Son beanOne = context.getBean(Son.class);
        Father beanTwo = context.getBean(Father.class);

        assertNotNull(beanOne);
        assertNotNull(beanTwo);
        assertEquals(beanOne, beanTwo);
    }

    // 2.1.3
    @Test
    void should_get_the_same_instance_with_the_abstract_class() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        DerivedClass beanOne = context.getBean(DerivedClass.class);
        AbstractBaseClass beanTwo = context.getBean(AbstractBaseClass.class);

        assertNotNull(beanOne);
        assertNotNull(beanTwo);
        assertEquals(beanOne, beanTwo);
    }

    // 2.2.1
    @Test
    void should_get_the_different_instance_when_the_scope_is_prototype() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        SimplePrototypeScopeClass beanOne = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass beanTwo = context.getBean(SimplePrototypeScopeClass.class);

        assertNotEquals(beanOne, beanTwo);
    }

    // 2.2.2
    @Test
    void should_prove_that_when_singleton_and_prototype_is_created() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        InterfaceOneImpl singletonOne = context.getBean(InterfaceOneImpl.class);
        InterfaceOneImpl singletonTwo = context.getBean(InterfaceOneImpl.class);
        SimplePrototypeScopeClass prototypeOne = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass prototypeTwo = context.getBean(SimplePrototypeScopeClass.class);

        assertEquals(singletonOne, singletonTwo);
        assertNotEquals(prototypeOne, prototypeTwo);
        assertArrayEquals(singletonOne.getLogger().stream().toArray(String[]::new), singletonTwo.getLogger().stream().toArray(String[]::new));
        assertNotEquals(prototypeOne.getLogger().stream().toArray(String[]::new), prototypeTwo.getLogger().stream().toArray(String[]::new));
    }

    // 2.2.3
    @Test
    void should_add_prototype_when_need_generate_bean_when_called() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        SimplePrototypeScopeClass prototypeOne = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass prototypeTwo = context.getBean(SimplePrototypeScopeClass.class);

        assertNotEquals(prototypeOne, prototypeTwo);
        assertNotEquals(prototypeOne.getLogger().stream().toArray(String[]::new), prototypeTwo.getLogger().stream().toArray(String[]::new));
    }

    // 2.2.4
    @Test
    void should_get_two_prototype_and_one_singleton() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        PrototypeScopeDependsOnSingleton prototypeOne = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton prototypeTwo = context.getBean(PrototypeScopeDependsOnSingleton.class);

        assertNotSame(prototypeOne, prototypeTwo);
        assertSame(prototypeOne.getSingletonDependent().getClass(), prototypeTwo.getSingletonDependent().getClass());
    }

    // 2.2.5
    @Test
    void should_get_one_singleton_and_one_prototype() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        SingletonDependsOnPrototype singletonOne = context.getBean(SingletonDependsOnPrototype.class);
        SingletonDependsOnPrototype singletonTwo = context.getBean(SingletonDependsOnPrototype.class);

        assertSame(singletonOne, singletonTwo);
        assertSame(singletonOne.getPrototypeDependent().getClass(), singletonTwo.getPrototypeDependent().getClass());
    }

    // 2.3.1
    @Test
    void should_create_prototype_calling_independent_method_using_proxy_mode() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        SingletonDependsOnPrototypeProxy beanOne = context.getBean(SingletonDependsOnPrototypeProxy.class);
        SingletonDependsOnPrototypeProxy beanTwo = context.getBean(SingletonDependsOnPrototypeProxy.class);

        assertNotNull(beanOne);
        assertNotNull(beanTwo);
        assertSame(beanOne, beanTwo);
        assertSame(beanOne.getPrototypeDependentWithProxy(), beanTwo.getPrototypeDependentWithProxy());
        assertSame(beanOne.getPrototypeDependentWithProxy().getThis(), beanTwo.getPrototypeDependentWithProxy().getThis());
        assertNotEquals(beanOne.getPrototypeDependentWithProxy().getThis().getRandom(), beanTwo.getPrototypeDependentWithProxy().getThis().getRandom());
    }

    // 2.3.2
    @Test
    void should_create_prototype_calling_not_independent_method_using_proxy_mode() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        SingletonDependsOnPrototypeProxyBatchCall bean = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);

        assertNotNull(bean);
        assertNotEquals(bean.getPrototypeScopeString(), bean.getPrototypeScopeString());
    }

    // 2.3.3
    @Test
    void should_prove_it_is_a_proxy() {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp");

        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        String actual = bean.getPrototypeDependentWithProxy().getClass().toString();

        assertTrue(actual.matches(".*EnhancerBySpringCGLIB.*"));
    }
}
